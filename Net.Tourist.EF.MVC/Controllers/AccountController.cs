﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Net.Tourist.EF.MVC.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="netpath">网络路径 eg:http://www.shunshun168.com/WS/QRCode.ashx?qt=http://www.shunshun168.com/Mobile/Register.aspx?param=100102&qm=&qs=144</param>
        /// <param name="downloadName">要保存的文件名 eg:qrcode.png</param>
        public void DownLoad(string netpath, string downloadName)
        {
          
            WebRequest request = WebRequest.Create(netpath);
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            List<byte> list = new List<byte>();
            byte[] buff = new byte[512];
            int c = 0; //实际读取的字节数
            while ((c = stream.Read(buff, 0, buff.Length)) > 0)
            {
                list.AddRange(buff);
            }

            HttpContext.Response.Clear();
            HttpContext.Response.Charset = "GB2312";
            HttpContext.Response.ContentEncoding = Encoding.UTF8;
            // 添加头信息，为"文件下载/另存为"对话框指定默认文件名 
            HttpContext.Response.AddHeader("Content-Disposition",
                                                   "attachment; filename=" +
                                                   HttpUtility.UrlEncode(downloadName, Encoding.UTF8));

            // 添加头信息，指定文件大小，让浏览器能够显示下载进度 
            HttpContext.Response.AddHeader("Content-Length", list.Count.ToString());
            // 指定返回的是一个不能被客户端读取的流，必须被下载 
            HttpContext.Response.ContentType = "image/jpeg";
            // 把文件流发送到客户端 
            HttpContext.Response.BinaryWrite(list.ToArray());
            HttpContext.Response.Flush();


        }
    }
}