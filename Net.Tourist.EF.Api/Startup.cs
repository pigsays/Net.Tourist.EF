﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using Microsoft.Owin;
using Net.Tourist.EF.OAuth2;
[assembly:OwinStartup(typeof(Net.Tourist.EF.Api.Startup))]

namespace Net.Tourist.EF.Api
{
    /// <summary>
    /// OWIN启动
    /// </summary>
    public partial class Startup
    {
        public void Configuration(IAppBuilder app) {
            ConfigureOAuth2(app);
        }
    }
}