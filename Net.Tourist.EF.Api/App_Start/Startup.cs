﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Net.Tourist.EF.OAuth2;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security;
using Microsoft.Owin;
using Net.Tourist.EF.OAuth2.Providers;

namespace Net.Tourist.EF.Api
{
    public partial class Startup
    {
        public void ConfigureOAuth2(IAppBuilder app) {
            //OAuthConfiguration config = new OAuthConfiguration();
            //config.Config(app);
            OAuthAuthorizationServerOptions server = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                AuthenticationMode = AuthenticationMode.Active,
                TokenEndpointPath = new PathString("/token"),
                AuthorizeEndpointPath = new PathString("/authorize"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(1),

                Provider = new OpenAuthorizationServerProvider(), ///access_token 授权服务
                AuthorizationCodeProvider = new OpenAuthorizationCodeProvider(), //认证码授权服务
                RefreshTokenProvider = new OpenRefreshTokenProvider(), //refresh_token 授权服务
            };
            app.UseOAuthBearerTokens(server);
        }
    }
}