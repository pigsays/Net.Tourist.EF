﻿using Net.Tourist.EF.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Net.Tourist.EF.Api.Controllers
{
    public class AccountController : ApiController
    {
        private HttpClient _httpClient = new HttpClient();
        private string _baseuri = "http://localhost:9999";
        private string _redirect_uri { get { return $"{_baseuri}/account/code"; } }
        public AccountController()
        {
            _httpClient.BaseAddress = new Uri(_baseuri);
        }
        [Route("account/code")]
        public string GetAuthorizeCode(string code)
        {
            return code;
        }

        private async Task<string> GetAuthorizationCode(string clientId, string redirect_uri)
        {

            var response = await _httpClient.GetAsync($"/authorize?grant_type=authorization_code&response_type=code&client_id={clientId}&redirect_uri={HttpUtility.UrlEncode(redirect_uri)}");
            var authorizationCode = await response.Content.ReadAsStringAsync();
            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }
            return authorizationCode;
        }
        /// <summary>
        /// 客户端授权
        /// </summary>
        /// <param name="appkey">appkey</param>
        /// <param name="appSecret">appSecret</param>
        /// <returns></returns>
        [Route("account/authorization_code")]
        [HttpGet]
        public async Task<TokenResponse> authorization_code(string appkey, string appSecret = "123456")
        {
            string code = await GetAuthorizationCode(appkey, _redirect_uri);

            if (!string.IsNullOrWhiteSpace(code))
            {
                code = code.Replace("\"", "");
                return await GetToken("authorization_code", null, appkey, appSecret, code);
            }
            return new TokenResponse();


        }
        /// <summary>
        /// 刷新Token
        /// </summary>
        /// <param name="refresh_token">refresh_token</param>
        /// <param name="appkey">appkey</param>
        /// <param name="appSecret">appSecret</param>
        /// <returns></returns>
        [Route("account/refresh_token")]
        [HttpGet]
        public async Task<TokenResponse> RefreshToken(string refresh_token, string appkey, string appSecret = "123456")
        {
            return await GetToken("refresh_token", refresh_token, appkey, appSecret, null);
        }

        /// <summary>
        /// 密码授权
        /// </summary>
        /// <param name="appkey">用户名</param>
        /// <param name="appSecret">密码</param>
        /// <returns></returns>
        [Route("account/password")]
        [HttpGet]
        public async Task<TokenResponse> Passport(string appkey, string appSecret = "123456")
        {
            return await GetToken("password", null, appkey, appSecret);
        }

        [Route("account/implicit")]
        [HttpGet]
        public async Task<HttpResponseMessage> Implicit(string appkey)
        {
           return await _httpClient.GetAsync($"/authorize?response_type=token&client_id={appkey}&redirect_uri={HttpUtility.UrlEncode($"{_redirect_uri}")}");
        }

        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="grantType">
        /// 授权类型
        /// 1.client_credentials 客户端授权
        /// 2.authorization_code 授权码授权
        /// 3.password           密码授权
        /// 4.refresh_token      刷新token授权
        /// 5.implicit           隐式授权
        /// </param>
        /// <param name="refreshToken">刷新token</param>
        /// <param name="client_id">用户名/client_id</param>
        /// <param name="client_secret">密码/clientSecret</param>
        /// <param name="authorizationCode">授权码</param>
        /// <returns></returns>
        private async Task<TokenResponse> GetToken(string grantType, string refreshToken = null, string client_id = null, string client_secret = null, string authorizationCode = null)
        {

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("grant_type", grantType);
            if (!string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(client_secret))
            {
                if (grantType.Equals("password"))
                {
                    parameters.Add("UserName", client_id);
                    parameters.Add("Password", client_secret);
                }
                parameters.Add("client_id", client_id);
                parameters.Add("client_secret", client_secret);

            }
            if (!string.IsNullOrEmpty(authorizationCode))
            {
                parameters.Add("code", authorizationCode);
                parameters.Add("redirect_uri", _redirect_uri); //和获取 authorization_code 的 redirect_uri 必须一致，不然会报错
            }
            if (!string.IsNullOrEmpty(refreshToken))
            {
                parameters.Add("refresh_token", refreshToken);
            }
            //_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            //    "Basic",
            //    Convert.ToBase64String(Encoding.ASCII.GetBytes(client_id + ":" + client_secret)));

            var response = await _httpClient.PostAsync("/token", new FormUrlEncodedContent(parameters));
            var responseValue = await response.Content.ReadAsStringAsync();
            if (response.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine(response.StatusCode);
                Console.WriteLine((await response.Content.ReadAsAsync<HttpError>()).ExceptionMessage);
                return null;
            }
            return await response.Content.ReadAsAsync<TokenResponse>();
        }

    }
}
