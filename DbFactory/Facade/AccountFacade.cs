﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;

namespace DbFactory.Facade
{
    public class AccountFacade
    {
        private readonly SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString);
        /// <summary>
        /// 是否存在Appkey
        /// </summary>
        /// <param name="appkey"></param>
        /// <returns></returns>
        public bool ValidatedClient(string appkey, string appsecret)
        {
            string query = $"select count(1) from users where appkey=@appkey and  appsecret=@appsecret and app_enable=1";
            return conn.ExecuteScalar<int>(query, new { appkey = appkey, appsecret = appsecret }) > 0;
        }

        public bool ValidatedClient(string appkey)
        {
            string query = $"select count(1) from users where appkey=@appkey  and app_enable=1";
            return conn.ExecuteScalar<int>(query, new { appkey = appkey }) > 0;
        }
        /// <summary>
        /// 用户是否存在
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userpwd"></param>
        /// <returns></returns>
        public bool ValidatedUser(string username, string userpwd)
        {

            string query = $"select count(1) from users where username=@username and  userpwd=@userpwd and user_enable=1";
            return conn.ExecuteScalar<int>(query, new { username = username, userpwd = userpwd }) > 0;
        }

        public bool ValidatedRedirectUri(string appkey, string redirecturi)
        {
            try
            {

                string query = $"select count(1) from users where appkey=@appkey and  redirecturi=@redirecturi and user_enable=1";
                return conn.ExecuteScalar<int>(query, new { appkey = appkey, redirecturi = redirecturi }) > 0;

            }
            catch (Exception ex)
            {

                return false;
            }
        }


    }
}
