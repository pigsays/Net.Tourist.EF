﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFactory.Models
{
    public class Tokens
    {
        public int id { get; set; }

        public string access_token { get; set; }

        public string refresh_token { get; set; }

        public int user_id { get; set; }

        public DateTime issued_utc { get; set; }

        public DateTime exprise_utc { get; set; }

        public string grant_type { get; set; }


    }
}
