﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFactory.Models
{
    public class Users
    {

        public int id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string userpwd { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public int age { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime brithday { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string avtor { get; set; }
        /// <summary>
        /// appkey
        /// </summary>
        public string appkey { get; set; }
        /// <summary>
        /// appsecret
        /// </summary>
        public string appsecret { get; set; }
        /// <summary>
        /// 跳转地址
        /// </summary>
        public string redirecturi { get; set; }
        /// <summary>
        /// 客户端授权是否启用
        /// </summary>
        public bool app_enable { get; set; }
        /// <summary>
        /// 用户是否启用
        /// </summary>
        public bool user_enable { get; set; }
    }
}
