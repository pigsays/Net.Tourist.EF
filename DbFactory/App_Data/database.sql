﻿USE master
IF EXISTS( SELECT *FROM sys.databases WHERE name ='OAuth2EF')
BEGIN
declare @dbname sysname  
set @dbname = 'OAuth2EF' --这个是要删除的数据库库名   <br>
 /*****用游标杀死进程*****/    
declare @s nvarchar(1000)  
declare tb cursor local 
for
    select s = 'kill   ' + cast(spid as varchar)
    from   master.dbo.sysprocesses
    where  dbid = DB_ID(@dbname)  
       
open   tb    
fetch   next   from   tb   into   @s  
while @@fetch_status = 0
begin
    exec (@s) 
    fetch next from tb into @s
end  
close   tb  
deallocate   tb  
  /*****删除数据库*****/
exec ('drop   database   [' + @dbname + ']') 
end


CREATE DATABASE OAuth2EF ON PRIMARY
(
NAME='OAuth2EF',
FILENAME='E:\DotNetPrj\Net.Tourist.EF\DbFactory\App_Data\OAuth2EF.mdf', --c:/data/oauth2ef.mdf,
SIZE=5MB,
MAXSIZE =150MB,
FILEGROWTH=20%
)
LOG ON 
(
   NAME ='OAuth2EF_log',
   FILENAME='E:\DotNetPrj\Net.Tourist.EF\DbFactory\App_Data\OAuth2EF.ldf',--c:/data/oauth2ef.ldf,
   SIZE=5MB,
   FILEGROWTH=5MB
   
)
go
USE OAuth2EF

CREATE TABLE users(
  id INT PRIMARY KEY IDENTITY NOT NULL,
  username NVARCHAR(100) NOT NULL,
  userpwd NVARCHAR(256) NOT NULL,
  age INT NOT NULL DEFAULT(0),
  brithday DATETIME NOT NULL DEFAULT(GETDATE()),
  avtor NVARCHAR(max) DEFAULT(''),
  appkey NVARCHAR(256) NOT NULL DEFAULT(''),
  appsecret NVARCHAR(300) NOT NULL DEFAULT(''),
  redirecturi NVARCHAR(MAX) NOT NULL DEFAULT(''),
  app_enable BIT NOT NULL DEFAULT(1),
  user_enable BIT NOT NULL DEFAULT(1)
  
)
CREATE TABLE tokens(
  id INT IDENTITY PRIMARY KEY NOT NULL,
  access_token NVARCHAR(300) NOT NULL,
  refresh_token NVARCHAR(256) NOT NULL,
  [user_id] INT NOT NULL,
  issued_utc DATETIME DEFAULT(GETDATE()) NOT NULL,
  exprise_utc DATETIME DEFAULT(DATEADD(HOUR,1,GETDATE())) NOT NULL,
  grant_type NVARCHAR(100) NOT NULL 


  
)

GO
INSERT INTO dbo.users
        ( username ,
          userpwd ,
          age ,
          brithday ,
          avtor ,
          appkey ,
          appsecret ,
          redirecturi,
          app_enable ,
          user_enable
        )
VALUES  ( N'uejar' , -- username - nvarchar(100)
          N'uejar' , -- userpwd - nvarchar(256)
          22 , -- age - int
          '1995-04-08' , -- brithday - datetime
          N'' , -- avtor - nvarchar(max)
          N'u2fsdgvkx19iodmb' , -- appkey - nvarchar(256)
          N'u2fsdgvkx18i8in8yli5autg6cm0' , -- appsecret - nvarchar(300)
          N'http://localhost:9999/account/code',
          1 , -- app_enable - bit
          1  -- user_enable - bit
        )
        
