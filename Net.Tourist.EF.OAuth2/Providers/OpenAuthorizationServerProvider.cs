﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using DbFactory.Facade;
namespace Net.Tourist.EF.OAuth2.Providers
{
    public class OpenAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly AccountFacade facade = new AccountFacade();
        /// <summary>
        /// access_token/authorize_code 认证
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task AuthorizeEndpoint(OAuthAuthorizeEndpointContext context)
        {
            if (context.AuthorizeRequest.IsImplicitGrantType)
            {
                ///access_token 授权
                ClaimsIdentity identity = new ClaimsIdentity("bearer");
                context.OwinContext.Authentication.SignIn(identity);
                context.RequestCompleted();
            }
            else if (context.AuthorizeRequest.IsAuthorizationCodeGrantType)
            {
                ///authorize_code 授权
                var redrict_uri = context.Request.Query["redirect_uri"];
                var clientId = context.Request.Query["client_id"];
                var identity = new ClaimsIdentity(new GenericIdentity(clientId, OAuthDefaults.AuthenticationType));
                AuthenticationTicket ticket = new AuthenticationTicket(
                    identity,
                    new AuthenticationProperties(new Dictionary<string, string>() {
                        { "client_id",clientId},
                        { "redirct_uri",redrict_uri}
                    })
                    {
                        IssuedUtc = DateTimeOffset.Now,
                        ExpiresUtc = DateTimeOffset.Now.Add(context.Options.AuthorizationCodeExpireTimeSpan)
                    }
                    );
                var authorizeCodeContext = new AuthenticationTokenCreateContext(
                    context.OwinContext,
                    context.Options.AuthorizationCodeFormat,
                    ticket);
                await context.Options.AuthorizationCodeProvider.CreateAsync(authorizeCodeContext);
                context.Response.Redirect($"{redrict_uri}?code={Uri.EscapeDataString(authorizeCodeContext.Token)}");
                context.RequestCompleted();
            }

        }

        /// <summary>
        /// 验证客户端信息
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string client_id = string.Empty, client_sercert = string.Empty;

            if (!context.TryGetBasicCredentials(out client_id, out client_sercert))
            {
                context.TryGetFormCredentials(out client_id, out client_sercert);
            }
            if (!facade.ValidatedClient(client_id, client_sercert))
            {
                context.SetError("invalid_client", "appkey 或 appsecret 有误");
                return;
            }
            context.Validated();
        }

        /// <summary>
        /// 验证授权请求
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateAuthorizeRequest(OAuthValidateAuthorizeRequestContext context)
        {
            if (facade.ValidatedClient(context.ClientContext.ClientId) &&
                (context.AuthorizeRequest.IsAuthorizationCodeGrantType || context.AuthorizeRequest.IsImplicitGrantType))
            {
                context.Validated();
            }
            else
            {
                context.Rejected();
            }
        }
        /// <summary>
        /// 验证跳转地址
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            //目前不验证,后续将rediect_url加入数据库联合client_id 进行验证
            if (facade.ValidatedRedirectUri(context.ClientId, context.RedirectUri))
            {
                context.Validated(context.RedirectUri);
            }
            else
            {
                context.Rejected();
            }

        }

        /// <summary>
        /// 验证access_token 请求
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateTokenRequest(OAuthValidateTokenRequestContext context)
        {
            if (context.TokenRequest.IsAuthorizationCodeGrantType
                || context.TokenRequest.IsClientCredentialsGrantType
                || context.TokenRequest.IsRefreshTokenGrantType
                || context.TokenRequest.IsResourceOwnerPasswordCredentialsGrantType)
            {
                context.Validated();
            }
            else
            {
                context.Rejected();
            }
        }

        public override async Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            var identity = new ClaimsIdentity(new GenericIdentity(
                context.ClientId, OAuthDefaults.AuthenticationType),
                context.Scope.Select(x => new Claim("urn:oauth:scope", x)));

            context.Validated(identity);
        }
        /// <summary>
        /// 生成 access_token（resource owner password credentials 授权方式）
        /// </summary>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (string.IsNullOrEmpty(context.UserName))
            {
                context.SetError("invalid_username", "username is not valid");
                return;
            }
            if (string.IsNullOrEmpty(context.Password))
            {
                context.SetError("invalid_password", "password is not valid");
                return;
            }

            if (!facade.ValidatedUser(context.UserName, context.Password))
            {
                context.SetError("invalid_identity", "username or password is not valid");
                return;
            }

            var OAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
            OAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            context.Validated(OAuthIdentity);
        }

    }
}